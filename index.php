<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.15.3/css/all.css" integrity="sha384-SZXxX4whJ79/gErwcOYf+zWLeJdY/qpuqC4cAa9rOGUstPomtqpuNWT9wdPEn2fk" crossorigin="anonymous">
    <link rel="stylesheet" href="public/frontend/css/style.css">
    <title>IT Future News Portal</title>
</head>
<body>
    <div class="container bg-dark text-white py-1">
        <div class="row">
            <div class="col-md-8">
                <span>Dhaka | Bangladesh</span>
                <span>| <?php echo date('l, d, M, Y')?></span>
                <span>| Today's News</span>
                <span>| E-Paper</span>
                <span>| Archive</span>
                <span>| Beta</span>
            </div>
            <div class="col-md-4 text-end">
                <div class="social-icon">
                    <a href=""> <i class="fab fa-facebook-square"></i></a>
                    <a href=""> <i class="fab fa-twitter-square"></i></a>
                    <a href=""> <i class="fab fa-youtube-square"></i></a>
                    <a href=""> <i class="fab fa-instagram-square"></i></a>
                    <a href=""> <i class="fab fa-linkedin"></i></a>
                </div>
            </div>
        </div>
    </div>
    <div class="container border-bottom">
        <div class="row">
            <div class="col-md-4">
                <div class="logo">
                    <a href="index.php"><img src="public/frontend/img/logo.png" alt="Logo" class="img-fluid"></a>
                </div>
            </div>
            <div class="col-md-8 text-end">
                <div class="menu">
                    <a href="" class="active">Covid-19</a>
                    <a href="">National</a>
                    <a href="">International</a>
                    <a href="">Economics</a>
                    <a href="">Spots</a>
                    <a href="">Entertainment</a>
                    <a href="">Education</a>
                    <a href="">Life Style</a>
                </div>
            </div>
        </div>
    </div>
    <div class="container mt-5">
        <div class="row">
            <div class="col-md-8">
                <a href="">
                    <div class="position-relative">
                        <img src="public/frontend/img/cover.jpg" class="img-fluid img-thumbnail" alt="feature image">
                        <div class="position-absolute bottom-0 p-3 feature-headline">
                            <h4>Feature Title</h4>
                        </div>
                    </div>
                </a>
                <div class="row">
                    <?php for($i=1; $i<=9; $i++){ ?>
                    <div class="col-md-4 mt-4">
                        <div class="news-container">
                            <a href="">
                                <div class="card">
                                    <img src="public/frontend/img/news.jpg" alt="news image" class="card-img-top">
                                    <div class="card-header">
                                        <h5>News Headline</h5>
                                    </div>
                                    <div class="card-body">
                                        <p>This is a news about Bangladesh This is a news about Bangladesh...</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <div class="col-4">
                <div class="card">
                    <div class="card-header">
                        <h4>Latest News</h4>
                    </div>
                    <div class="card-body">
                        <?php for($i=1;$i<=10 ; $i++){ ?>
                        <div class="row mt-2 pb-2 border-bottom">
                            <div class="col-4">
                               <a href=""> <img src="public/frontend/img/latest.jpg" alt="Latest News" class="img-fluid img-thumbnail"></a>
                            </div>
                            <div class="col-8">
                                <a href="" class="default-link">Lorem ispum Lorem ispum Lorem ispum</a>
                            </div>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<!-- Optional JavaScript; choose one of the two! -->

<!-- Option 1: Bootstrap Bundle with Popper -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>

<!-- Option 2: Separate Popper and Bootstrap JS -->
<!--
<script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js" integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.min.js" integrity="sha384-Atwg2Pkwv9vp0ygtn1JAojH0nYbwNJLPhwyoVbhoPwBhjQPR5VtM2+xf0Uwh9KtT" crossorigin="anonymous"></script>
-->
</body>
</html>